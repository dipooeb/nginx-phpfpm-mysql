# Installation

### Configuration
Créer un fichier `.env` et copier/coller le contenu du `.env.example` à l'intérieur. 
Modifier les laveurs des variables d'environnement au besoin.

### Projet

Déposer le contenu du projet symfony dans le dossier `src/symfony`.

### Lancement du `docker`

> Toutes les commandes sont disponibles dans le `Makefile`.

#### Lancement des containers Nginx/PHP-fpm, MySQL & Adminer
```bash
make start
````

#### Installation des dépendances de `composer`
```bash
make composer-i
````

#### Ajout un composant au `vendor`
```bash
make composer-req vendor="mon/vendor"
````
