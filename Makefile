#!make
include ./.env

.PHONY: composer-i create-sf

composer-i:
	docker exec -i "${COMPOSE_PROJECT_NAME}_php" sh -c 'composer install --dev --prefer-dist --no-autoloader --no-scripts --no-progress --working-dir=/var/www/html/symfony'

composer-req:
	docker exec -i "${COMPOSE_PROJECT_NAME}_php" sh -c 'composer req $(vendor) --working-dir=/var/www/html/symfony'

create-sf:
	docker exec -i "${COMPOSE_PROJECT_NAME}_php" composer create-project symfony/skeleton:"$(version)" symfony

start:
	docker-compose up -d --force-recreate --build --remove-orphans
